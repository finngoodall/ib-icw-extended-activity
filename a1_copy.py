"""
Copy of python code for lab a1, editied to be used for our extended activity
"""


import argparse
import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt


def MLKF_1dof(m, l, k, f1):

    """Return mass, damping, stiffness & force matrices for 1DOF system"""

    M = np.array([[m]])
    L = np.array([[l]])
    K = np.array([[k]])
    F = np.array([f1])

    return M, L, K, F


def MLKF_2dof(m, l, k, f1, f2):

    """Return mass, damping, stiffness & force matrices for 2DOF system"""

    M = np.array([[m, 0], [0, m]])
    L = np.array([[2*l, -l], [-l, l]])
    K = np.array([[2*k, -k], [-k, k]])
    F = np.array([f1, f2])

    return M, L, K, F


def MLKF_3dof(m, l, k, f1, f2, f3):

    """Return mass, damping, stiffness & force matrices for 3DOF system"""

    M = np.array([[m, 0, 0], [0, m, 0], [0, 0, m]])
    L = np.array([[2*l, -l, 0], [-l, 2*l, -l], [0, -l, 2*l]])
    K = np.array([[2*k, -k, 0], [-k, 2*k, -k], [0, -k, 2*k]])
    F = np.array([f1, f2, f3])

    return M, L, K, F


def freq_response(w_list, M, L, K, F):

    """Return complex frequency response of system"""

    return np.array(
        [np.linalg.solve(-w*w * M + 1j * w * L + K, F) for w in w_list]
    )


def time_response(t_list, M, L, K, F):

    """Return time response of system"""

    mm = M.diagonal()

    def slope(t, y):
        xv = y.reshape((2, -1))
        a = (F - L@xv[1] - K@xv[0]) / mm        # finds acceleration of the mass
        s = np.concatenate((xv[1], a))          # [array of xv(?)] + [array of accelerations]
        return s

    solution = scipy.integrate.solve_ivp(
        fun=slope,                              # what does slope function have as its inputs? t and y are not defined
        t_span=(t_list[0], t_list[-1]),
        y0=np.zeros(len(mm) * 2),
        method='Radau',
        t_eval=t_list
    )

    return solution.y[0:len(mm), :].T


def last_nonzero(arr, axis, invalid_val=-1):

    """Return index of last non-zero element of an array"""

    mask = (arr != 0)
    val = arr.shape[axis] - np.flip(mask, axis=axis).argmax(axis=axis) - 1
    return np.where(mask.any(axis=axis), val, invalid_val)


def plot(hz, sec, M, L, K, F):

    """Plot frequency and time domain responses"""

    # Generate response data

    f_response = np.abs(freq_response(hz * 2*np.pi, M, L, K, F))
    t_response = time_response(sec, M, L, K, F)

    # Determine suitable legends

    f_legends = (
        'm{} peak {:.4g} metre at {:.4g} Hz'.format(
            i+1,
            f_response[m][i],
            hz[m]
        )
        for i, m in enumerate(np.argmax(f_response, axis=0))
    )

    equilib = np.abs(freq_response([0], M, L, K, F))[0]         # Zero Hz
    toobig = abs(100 * (t_response - equilib) / equilib) >= 2
    lastbig = last_nonzero(toobig, axis=0, invalid_val=len(sec)-1)

    t_legends = (
        'm{} settled to 2% beyond {:.4g} sec'.format(
            i+1,
            sec[lastbig[i]]
        )
        for i, _ in enumerate(t_response.T)
    )

    # Create plot


    fig, ax = plt.subplots(1, 1, figsize=(11.0, 7.7))

    ax.set_title('Frequency domain response')
    ax.set_xlabel('Frequency/hertz')
    ax.set_ylabel('Amplitude/metre')
    ax.legend(ax.plot(hz, f_response), f_legends)

    #ax.set_title('Time domain response')
    #ax.set_xlabel('Time/second')
    #ax.set_ylabel('Displacement/metre')
    #ax.legend(ax.plot(sec, t_response), t_legends)

    fig.tight_layout()
    plt.show()


def main():

    """Main program"""

    # Parse arguments

    ap = argparse.ArgumentParser('Plot response curves')

    ap.add_argument('--dof', type=int, default=1, help='DoF [1]')

    ap.add_argument('--m', type=float, default=67500, help='Mass [67500]')
    ap.add_argument('--l', type=float, default=106e3, help='Damping [0.00]')
    ap.add_argument('--k', type=float, default=103.2e6, help='Spring [103.2e6]')

    ap.add_argument('--f1', type=float, default=0.25, help='Force 1 [12500]')
    ap.add_argument('--f2', type=float, default=0.0, help='Force 2 [0.0]')
    ap.add_argument('--f3', type=float, default=0.0, help='Force 3 [0.0]')

    ap.add_argument(
        '--hz', type=float, nargs=2, default=(0, 20),
        help='Frequency range [0 20]'
    )
    ap.add_argument(
        '--sec', type=float, default=75,
        help='Time limit [75]'
    )

    args = ap.parse_args()

    # Generate matrices describing the system

    if args.dof == 1:
        M, L, K, F = MLKF_1dof(
            args.m, args.l, args.k, args.f1
        )
    elif args.dof == 2:
        M, L, K, F = MLKF_2dof(
            args.m, args.l, args.k, args.f1, args.f2
        )
    else:
        M, L, K, F = MLKF_3dof(
            args.m, args.l, args.k, args.f1, args.f2, args.f3
        )

    # Generate frequency and time arrays

    hz = np.linspace(args.hz[0], args.hz[1], 10001)
    sec = np.linspace(0, args.sec, 10001)

    # Plot results

    plot(hz, sec, M, L, K, F)


if __name__ == '__main__':
    main()
