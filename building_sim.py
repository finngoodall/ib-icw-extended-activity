"""
Code used for simulating response of buildings to earthquakes
"""


import argparse
import numpy as np
import scipy.integrate
from scipy.io import loadmat
import matplotlib.pyplot as plt

def get_quake(file, samplerate=500):

    """
    Gets the earthquake data from one of the .mat files on moodle
    Takes the .mat file as in input, returns a numppy array of [[quake data points],[time]]
    """

    quake_values = loadmat(file).get('f0')
    quake_time_array = np.zeros((2,37500))

    for i in range(0,37500):
        quake_time_array[0,i] = quake_values[i]
        quake_time_array[1,i] = i/samplerate

    return quake_time_array

def quake_plot(file, samplerate=500):

    """Plots the earthquake data along time"""

    quake_time_array = get_quake(file)
    x = quake_time_array[1,:]
    y = quake_time_array[0,:]

    plt.plot(x, y)
    plt.title("earthquake amplitude")
    plt.xlabel("time /s")
    plt.ylabel("earthquake amplitude /mm")

    plt.show()

def quake_fourier(input_array, samplerate=500):

    """
    Performs a fourier transform on the earthquake data
    Takes an input array of the data from an earthquake, and plots fourier transform
    Can be used to find important freqencies in a quake
    """

    # fourier transform and get fequency values for axis
    y_fourier = np.fft.fft(input_array)
    freq = np.fft.fftfreq(len(y_fourier), 1/samplerate)
    

    plt.plot(freq[:len(y_fourier)//2], np.abs(y_fourier[:len(y_fourier)//2]))
    plt.title("earthquake fourier transform")
    plt.xlabel("frequency [Hz]")
    plt.ylabel("amplitude [mms]")
    
    plt.show()


def impulse_response(t_array, zeta=0.5, w_n=1*2*np.pi):

    """
    Response of system to unit impulse
    For model, treat input signa as series of impluses
    """

    w_d = np.sqrt(1-zeta**2)
    
    y_array = np.zeros(len(t_array))

    for i in range(len(t_array)):
        t_i = t_array[i]
        if zeta == 1:
            #critical damping response
            y = w_n**2 * t_i * np.exp(-w_n * t)
        else:
            #non-critical damping response
            y = (w_n * np.exp(-zeta * w_n * t_i) * np.sin(w_d * t_i)) / w_d
        y_array[i] = y

    return y_array


def plot_shaking(file):

    """
    Plots the ground and building shake from an earthquake
    """

    f = get_quake(file)
    g = impulse_response(t_array=f[1,:], zeta = 0.10, w_n = 2*np.pi*6.16)
    building_response = np.convolve(g, f[0,:])

    building_shake = plt.plot(f[1,:], building_response[:len(f[1,:])])
    
    plt.ylabel("Displacement [mm]")
    plt.xlabel("Time [s]")

    plt.show()


def main():

    quake_fourier(get_quake("dataset3-large.mat")[0,:])
    #quake_plot("dataset3-large.mat")
    #plot_shaking("dataset1-small.mat")


if __name__ == '__main__':
    main()
